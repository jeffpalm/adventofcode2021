module github.com/spudtrooper/adventofcode

go 1.17

replace github.com/spudtrooper/goutil v0.1.17 => "../goutil"

require (
	github.com/fatih/color v1.13.0
	github.com/go-errors/errors v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/spudtrooper/goutil v0.1.17
	github.com/thomaso-mirodin/intmath v0.0.0-20160323211736-5dc6d854e46e
)

require (
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
